from aufgabe2 import find_single_element

def test_negative():
    assert(find_single_element([-42,-23,-23]) == -42)
