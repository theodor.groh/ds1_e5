
import pytest
from aufgabe2 import find_single_element

def test_empty():
    with pytest.raises(Exception):
        assert(find_single_element([42,23,23,42]))
