# return single element or 0
def find_single_element(array: [int]) -> int:
    if not array or len(array)%2 == 0:
        raise ValueError("Array length must be odd")

    if len(array) == 0:
        raise ValueError("Array must contain values")

    res = 0
    for val in array:
        res ^= val

    return res

if __name__ == "__main__":
    array = [1,2,3,4,3,1,2]
    result = find_single_element(array)
    print(f"Der array hat folgende elemente : {array}")
    print(f"Das einzelne Element ist : {result}")
